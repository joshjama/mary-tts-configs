#!/bin/bash
# Installing the Mary TTS voices with the official Mary TTS installer 
MARY_HOME_PATH="/var/lib/.marytts"
VOICE_INSTALL() {
  cd $MARY_HOME_PATH/marytts-installer
  for ITER in $(cat $MARY_HOME_PATH/marytts-installer/marytts-voices.list)
  do 
    INSTALL_PARAM="install "$ITER
    echo "INSTALL_PARAM : "$INSTALL_PARAM 2>&1
    $MARY_HOME_PATH/marytts-installer/marytts $INSTALL_PARAM 2>&1
  done
}

VOICE_INSTALL 2>&1
