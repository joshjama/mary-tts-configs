#!/bin/bash
# Script to speek text with Mary TTS easily.
## It was created for test perposes.
## Run the Mary TTS server first.
# fetch url  https://github.com/marytts/marytts.git
INPUT_TOSAY=$(echo $@ |sed s'/ /\%20/g')

#LANGUAGE_TOSAY=$(echo $nf | tr -s " "  | sed 's/ //g' )
LANGUAGE_TOSAY=en_US
echo "Input" " " $INPUT_TOSAY
echo LANGUAGE $LANGUAGE_TOSAY
curl \
 http://localhost:59125/process\?INPUT_TYPE\=TEXT\&AUDIO\=WAVE_FILE\&OUTPUT_TYPE\=AUDIO\&LOCALE\=$LANGUAGE_TOSAY\&INPUT_TEXT\=%22$INPUT_TOSAY\!%22 \
 | aplay 
 echo "done"
