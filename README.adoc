= README 

autor=joshjama
version=0.9

== Mary TTS Configs 
.Whats that ?
* Mary TTS is a speech system build by the DFKI in cooperation with the university
Sahrland.
It can be used to process Text To Speech output from a given plain text.
Thanks to the huge efforts of the mary tts developers the provided voices are
more and more human like sounding.
They are available in a whide variety of languages and can be used for free.
This integration makes this voices available to every linux user which has git,
curl and a package manager installed on there distribution.
It enables the user to use the Mary TTS Speech-Technology together with
screenreaders like Orca.

.Why to use it ?
* Especialy blind and vissualy impared people rely on speech syntheses to us
there computers.
Thanks to the work of the orca and speech-dispatcher developers, blind users are
capable to use Linux systems for their dayly work nowadays. And as the Linux
desktop evolves more and more to the markets they get more and more used also by
non nerd users.
Thank you all, for making this open source and easy to use possible.
For the meantime we blind users use it with espeak, voxin or pico tts as our
Interface to our PCs.
While that is surely working well, 
Mac OS and Windows - users got more and more human sounding highquality
speech solutions shipping by different companies over the last decate.
Until now, it is very hard for a non nerdy user, and even complicated for a
technical interested noop,  to install such highquality 
voices on linux. You need to search them for long, have luck that the
installation runns by, and if it installs, it is even paid.
I think this is not what we want a critical main part of the interface to our
open source operating systems Userinterface to be. Please be aware, that this
should not be  a
critic on speech-dispatcher, espeak, pico, voxin, or even the orca maintiners. 
You are doing a very important work to make the linux desktop accessible, and
your solutions are culminating in a working desktop experience. 
My point is now, that I want to make this experience as beautifull as possible,
and as accessible as possible for as many beeings on the universe as possiblea




== Building and install 
On any debian-based system, you can just run
$ ./install.sh 
as root, and everything should work out of the box.
The installation scripts for distribution independent install are in progress
and will be integrated in the ./install.sh script with the next updates.

== Usage 
You can use the mary-generic module in orca.
Just select it and it should work.
* orca-preferences - voice tab -> select speech-dispatcher as system and
 mary-generic as syntesizer.
* You can eaver use the 
$ ./speek-text-with-marytts.sh <YOUR_TEXT>
script to speek any plain text with the english default voice.
* You can use your prefered web-browser on 
localhost:59125 
to reach the mary web frontend. There you can paste any text you want, and
select a voice to speek it.




== Requirements 

.Requirements 
* git 
* curl 
* sox 
* openjdk @version 8 or higher
* optional 
** Desktopenvironement with speech-dispatcher,
** screenreader 


== Components 

.Components 
* Mary TTS Server (java)
** The mary TTS Engine is based on a java server, running in the background
awaiting requests over curl.
The User can request the server to process a given Text and to give back a wave
file as output.
The user must play the resulted wave.
* Mary user 
** The Mary TTS server runns on a service called by a low privileged Mary TTS
user called .marytts.
* Service to run the mary server automaticaly and keep it alife.
* Speech-Dispatcher, orca and Other configuration
** Speech-Dispatcher 
*** The speechd.conf with the mary-generic module enabled
*** The module mary-generic.conf with the 30 available voices to make them
selectable by orca
** Speekup (Plan for future releases)



== TODO 
include ./TODO.adoc 



