#!/bin/bash
#
## @author joshjama
## @version 1.0
## @licence GNU Public licence v 2.0 @ Thanks to the Mary TTS Developers  (This should ot tuch the rights of any third parties envolved in this project,)
## 
## This project installs the Mary TTS voices and integrates them with 
## the orca screen reader.
## It downloads and installs all known available voices.
## It creats a .marytts user to run a Mary TTS server.
#
# Environement
MARY_HOME_PATH="/var/lib/.marytts"
CONFIG_REPO_PATH=$(pwd)
echo "CONFIG_REPO_PATH : " $CONFIG_REPO_PATH 
#Enshure that we are using it from the write location.
echo $CONFIG_REPO_PATH | grep "mary-tts-configs" 
if [[ $? -eq 0 ]]
then 
  echo "Installing ... "
else 
  
  echo "Sorry, please execute the installation script with in your git
  repository an with root privileges ."
  exit 1
fi 

# Giving the configuration folders and files to the .mary tts user in order to
# allow it to execute them.
GRANT_PERMISSION_TO_MARY_USER() {
  chown -R .marytts /var/lib/.marytts
  chown .marytts /var/log/marytts/
  chmod 700 /var/log/marytts
}

#Install requirements 
INSTALL_REQUIREMENTS() {
  apt-get update && apt-get -y upgrade 
  apt-get install -y wget curl tail
  apt-get install -y openjdk-11-jdk 
  apt-get install -y sox
  apt-get install -y openssl pgp
}

# Creating the .mary tts user in order to ensure security.
CREATE_USER() {
  #RANDOM_PW=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 8 | head -n 1)
  RANDOM_PW_OPENSSL=$( openssl rand --base64 24 )
  RANDOM_PW_GPG=$( gpg --gen-random --armor 1 14 |tail -n 1 )
  RANDOM_PW=$RANDOM_PW_OPENSSL""$RANDOM_PW_GPG
  useradd -r -m -d $MARY_HOME_PATH -p $RANDOM_PW -s /bin/bash .marytts 
  mkdir -p /var/log/marytts/
  GRANT_PERMISSION_TO_MARY_USER 2>&1 
  echo "Continueing "
}

# Install the service to run the mary tts server.
INSTALL_SERVICE() {
  # Install the tstartup programm for the mary tts server.
  cp $CONFIG_REPO_PATH/service/maryttsStartServer.sh /usr/local/bin/
  # Give the startup programm to the .marytts user.
  chown .marytts /usr/local/bin/maryttsStartServer.sh
  # Make it executable.
  chmod 750 /usr/local/bin/maryttsStartServer.sh

  # Install the service.
  cp $CONFIG_REPO_PATH/service/marytts.service /etc/systemd/system/
  # Enable the service.
  systemctl enable marytts 
  # Start you mary TTS server : 
  systemctl start marytts &
  echo "Installation finished."
}

# Server installation of the mary tts server :
#
## Download and install the newest version of the voice files 
## Note ! This needs improvement and is therefore currently not in use.
#VOICE_UPDATE() {
#  GRANT_PERMISSION_TO_MARY_USER 2>&1
#  #* After a successfull build.
#  echo "MARY_HOME_PATH : " $MARY_HOME_PATH
#  #New variant with the official marytts-installer 
#
#  cp $CONFIG_REPO_PATH/VOICE_UPDATE.sh /var/lib/.marytts/
#  runuser .marytts -c /var/lib/.marytts/VOICE_UPDATE.sh 2>&1
#    #./marytts 'install:*'
#  
#  chown -R .marytts /var/lib/.marytts/
#  echo "Continueing "
#}
# Handel runtime paramters.
if [[ $1 = "-update" ]] || [[ $1 = "-up" ]] || [[ $1 = "-u" ]] || \
  [[ $1 = "--update" ]]
then 
  #VOICE_UPDATE
  echo "Sorry, the VOICE_UPDATE function needs improvement and is therefore currently disabled."
  exit
fi

# Install the server :
MARYTTS_SERVER_INSTALL() {
  cd $MARY_HOME_PATH
  # Getting the marytts installer.
  git clone https://github.com/marytts/marytts-installer.git
  # Copy the list of known voices.
  cp $CONFIG_REPO_PATH/lib/marytts-voices.list $MARY_HOME_PATH/marytts-installer/
  cp $CONFIG_REPO_PATH/voice-install.sh $MARY_HOME_PATH/marytts-installer/
  # Giving the voices list to the new .marytts user.
  chown .marytts $MARY_HOME_PATH/marytts-installer/marytts-voices.list 
  # Giving the voice installer script to the .marytts user.
  chown .marytts $MARY_HOME_PATH/marytts-installer/voice-install.sh
  echo $MARY_HOME_PATH/marytts/
  # Download all known voices.
  #* Do to security enhawncements we can not run this in the same script. To run
  # it with the .marytts user we have to use a extra file ./voice-install.sh.
  GRANT_PERMISSION_TO_MARY_USER 2>&1
  runuser .marytts -c /var/lib/.marytts/marytts-installer/voice-install.sh 2>&1
  # Ensuring user rights 
  GRANT_PERMISSION_TO_MARY_USER 2>&1
  echo "Server-Installation done. "
  echo "Continueing "
}

# Initialize the installation of all components.
INITIALIZE_INSTALL() {

  CREATE_USER 2>&1
  INSTALL_REQUIREMENTS 2>&1 
  MARYTTS_SERVER_INSTALL 2>&1 
  INSTALL_SERVICE  2>&1 

}

INITIALIZE_INSTALL 2>&1 

# State the exsistance of the installation of mary tts.
if [[ $? -eq 0 ]]
then 
  touch $MARY_HOME_PATH/EXISTANCE 2>&1 
fi

#
